CC=gcc
COPTS=-Wno-pointer-to-int-cast
POSTOPTS=-larchive

default: withdir

nodir:
	$(CC) $(COPTS) -D NO_DIR_VAL -g main.c util.c json/json.c package.c validate.c filemap.c $(POSTOPTS) -o bin/rpack.exe

withdir:
	$(CC) $(COPTS) -g main.c util.c json/json.c validate.c package.c filemap.c $(POSTOPTS) -o bin/rpack.exe