#include "package.h"
#include <archive.h>
#include <archive_entry.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <fcntl.h>
#include "util.h"
#include "filemap.h"

dir_node *_getFileList()
{
    dir_node *list = dir_addFile(NULL, "pack.json");
    list = dir_mapList(list, "commands");
    list = dir_mapList(list, "install");
    return list;
}

void writePackage(const char *namespace)
{
    logvf("INFO: Generating filename\n");
    char *outname = malloc(strlen(namespace) + 6);
    sprintf(outname, "%s.rpkg", namespace);
    logvf("INFO: Generated filename: %s\n", outname);

    dir_node *files = _getFileList();
    struct archive *a;
    struct archive_entry *entry;
    struct stat st;
    char buff[8192];
    int length;
    int fd;

    a = archive_write_new();
    archive_write_add_filter_gzip(a);
    archive_write_set_format_pax_restricted(a);
    archive_write_open_filename(a, outname);
    while(files != NULL) {
        stat(files->path, &st);
        entry = archive_entry_new();
        archive_entry_set_pathname(entry, files->path);
        archive_entry_set_size(entry, st.st_size);
        archive_entry_set_filetype(entry, AE_IFREG);
        archive_entry_set_perm(entry, 0755);
        archive_write_header(a, entry);
        fd = open(files->path, O_RDONLY);
        length = read(fd, buff, sizeof(buff));
        while(length > 0)
        {
            archive_write_data(a, buff, length);
            length = read(fd, buff, sizeof(buff));
        }
        close(fd);
        archive_entry_free(entry);
        files = files->next;
    }
    archive_write_close(a);
    archive_write_free(a);
    printf("INFO: Successfully generated package %s\n", outname);

    free(outname);
}