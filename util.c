#include "util.h"
#include <string.h>
#include <stdarg.h>

static ARGS g_args;

const char *json_tstr[7] = {"json_object", "json_array", "json_integer", "json_double", "json_string", "json_boolean", "json_null"};

/**
 * Gets the default settings if the program is given no switches
 * 
 * @returns an args struct with verbose and quiet set to false.
 */
ARGS _getDefaultArgs()
{
    ARGS args;
    args.verbose = false;
    args.quiet = false;
    return args;
}

/**
 * Parses arguments passed to the application at runtime.
 * Provides options for verbose and quiet output. ARGS are
 * available via getArgs()
 * 
 * @param argc the number of args passed to the program
 * @param argv the value of all the args passed to the program
 */
void parseArgs(int argc, char *argv[])
{
    g_args = _getDefaultArgs();

    if (argc > 1)
    {
        for (int i = 1; i < argc; i++)
        {
            if (strcmp("--help", argv[i]) == 0)
            {
                printf("SYNTAX: rpack [OPTIONS] [FILE]\n");
                printf("OPTIONS:\n");
                printf("\t-v - Print verbose output\n");
                printf("\t-q - No interaction. All prompts will default to yes.\n");
                printf("FILE: The path the output package file.\n");
                exit(0);
            }
        }
        if (argc == 2)
        {
            for(int i = 1; i < argc; i++)
            {
                if(argv[i][0] == '-')
                {
                    parseSwitch(argv[i]);
                }
                else
                {
                    fprintf(stderr, "Invalid Syntax\n");
                    exit(-1);
                }
            }
        }
    }
}

/**
 * Parses a string which contains a command line switch
 * The program exits if the switch is not found. Switches
 * can be combined or separated, in typical GNU fashion.
 * For example rpack -v -q is the same as rpack -vq.
 * 
 * @param sw the switch being parsed
 */
void parseSwitch(char *sw)
{
    size_t swlen = strlen(sw);
    for(int i = 0; i < swlen; i++)
    {
        
        if(sw[i] == 'v')
        {
            g_args.verbose = true;
        }
        else if(sw[i] == 'q')
        {

            g_args.quiet = true;
        }
        else if(sw[i] != '-')
        {
            printf("ERROR: Unrecognized switch: %c\n", sw[i]);
            exit(-1);
        }
    }
}

/**
 * Gets the args that were initially passed to the program.
 * 
 * @pre parseArgs has been run
 * @returns the global args object.
 */
ARGS getArgs()
{
    return g_args;
}

/**
 * Reads pack.json from the PWD. If it cannot
 * be found, raises an error and exits the program.
 * 
 * @returns a string containing the contents of pack.json
 */
char *loadPackfile()
{
    FILE *f = fopen("pack.json", "rb");
    if (errno == 2)
    {
        fprintf(stderr, "ERROR: \'pack.json\' not found in directory.");
        exit(-1);
    }
    fseek(f, 0, SEEK_END);
    size_t flen = ftell(f);
    rewind(f);

    char *buf = malloc(flen + 1);
    fread(buf, 1, flen, f);
    fclose(f);

    buf[flen] = 0;

    return buf;
}

/**
 * Asks the user for a yes or no reponse with the given
 * prompt. Will not show if quiet is set
 * 
 * @param prompt the text the user will be prompted with
 * 
 * @returns false if the user types 'n' or 'N', otherwise, or if
 *          quiet is set, true.
 */
bool prompt(char *prompt)
{
    if(!g_args.quiet)
    {
        printf("%s [Y/n]: ", prompt);
        char input = getchar();
        return input != 'n' && input != 'N';
    }
    else
    {
        return true;
    }
}

/**
 * Logs to stdout if verbose is not set, otherwise
 * does nothing. Syntax is the same as printf
 */
void logvf(char *format, ...)
{
    if (g_args.verbose && !g_args.quiet)
    {
        va_list args;
        va_start(args, format);
        vprintf(format, args);
        va_end(args);
    }
}

/**
 * Logs to given FILE if verbose is set, otherwise
 * does nothing. Syntax is the same as fprintf
 */
void flogvf(FILE *_Stream, char *format, ...)
{
    if (g_args.verbose && !g_args.quiet)
    {
        va_list args;
        va_start(args, format);
        vfprintf(_Stream, format, args);
        va_end(args);
    }
}

/**
 * Returns a string representation of a given json_type
 * 
 * @param type the type being translated
 */
const char *js_tstr(json_type type)
{
    return json_tstr[type - 1];
}