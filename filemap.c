#include "filemap.h"
#include <stdlib.h>
#include <dirent.h>
#include <sys/stat.h>
#include <string.h>
#include <stdbool.h>
#include "util.h"

/**
 * Adds a file to the given directory listing. It adds new files to the
 * head of the linked list so that the efficientcy is O(1) rather than O(n)
 * 
 * @param list The head of the given directory listing. For a new list, leave
 *             this as NULL
 * @param path The path of the node being added.
 * 
 * @returns the new head of the list
 */
dir_node *dir_addFile(dir_node *list, char *path)
{
    dir_node *node = malloc(sizeof(dir_node));
    node->next = list;
    node->path = path;

    return node;
}

/**
 * Gets the size of the given list
 * 
 * @param list the head of the list
 * 
 * @returns the number of elements in the linked list
 */
int dir_listSize(dir_node *list)
{
    int count = 0;
    while(list != NULL)
    {
        count++;
        list = list->next;
    }

    return count;
}

/**
 * Frees all memory occupied by the given dirlist
 * 
 * @param list the head of the list
 */
void dir_delList(dir_node *list)
{
    while(list != NULL)
    {
        free(list->path);
        dir_node *prev = list;
        list = list->next;
        free(prev);
    }
}

/**
 * Combines two strings together with a '/' in between. These strings
 * will need to be freed manually as they are on the heap.
 */
char *_strAdd(char *str1, char *str2)
{
    char *resstr = malloc((strlen(str1) + strlen(str2) + 2) * sizeof(char));
    sprintf(resstr, "%s/%s", str1, str2);
    return resstr;
}

bool _isDir(char *path)
{
    struct stat s;
    if((stat(path, &s) == 0) && (s.st_mode & S_IFDIR))
    {
        return true;
    }
    return false;
}

/**
 * Maps the directory given in dir path to a linked list containing
 * all the files and their relative paths do the directory above dirpath.
 * So, if dirPath is "commands", subfiles of commands would include
 * 
 * @param list A list which you can append new entries to. If you want
 *             a new list, just set this to NULL
 * @param dirPath The path to be scanned and added to the list.
 * 
 * @returns the root node of the file. 
 */
dir_node *dir_mapList(dir_node *list, char *dirPath)
{
    logvf("INFO: Mapping directory %s\n", dirPath);
    struct dirent *dir_entry;
    DIR *target = opendir(dirPath);

    if(target == NULL)
    {
        flogvf(stderr, "WARNING: Directory %s does not exist. Continuing\n", dirPath);
        return list;
    }

    while((dir_entry = readdir(target)) != NULL)
    {
        if(strcmp("..", dir_entry->d_name) != 0 && strcmp(".", dir_entry->d_name) != 0)
        {
            char *relPath = _strAdd(dirPath, dir_entry->d_name);
            if(_isDir(relPath))
            {
                list = dir_mapList(list, relPath);
                free(relPath); // only free memory if not added to the list
            }
            else
            {
                dir_node *newNode = malloc(sizeof(dir_node));
                list = dir_addFile(list, relPath);
            }
        }
    }
    return list;
}