#include "validate.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include <unistd.h>

// Logging functions
/**
 * If verbose is set, writes an error in a predetermined format to be used
 * when one type is expected and another is recieved.
 * 
 * @param subject the item which failed validation (generally gotten via getJSRef)
 * @param exp the expected type
 * @param rec the type actually recieved
 */
void _logTypeError(const char *subject, json_type exp, json_type rec)
{
    flogvf(stderr, "ERROR: Failed to validate %s. Expected %s but recieved %s\n", subject, js_tstr(exp), js_tstr(rec));
}

// Helpers for getJSRef
/**
 * Gets the width of the string representation of the value
 * 
 * @param input the value whose width is being computed
 * 
 * @returns an unsinged int with the length of a string containing input
 */
size_t _getIntWidth(size_t input)
{
    if(input == 0)
    {
        return 1;
    }
    else
    {
        return floor(log10(input)) + 1;
    }
}

/**
 * Gets the index of a given json_value in its parent array
 *
 * @param val The element whose index is being retrieved
 * 
 * @pre val->parent->type is json_array
 * @returns the index of val in its parent
 */
size_t _jsGetIndex(json_value *val)
{
    for(size_t i = 0; i < val->parent->u.array.length; i++)
    {
        if(val->parent->u.array.values[i] == val)
        {
            return i;
        }
    }
    return 0;
}

/**
 * Gets the name of a property in its parent object
 * 
 * @param val The property whose name is being retrieved
 * 
 * @pre val->parent->type is json_object
 * @returns a string containing the name of the property in its parent
 */
char *_jsGetName(json_value *val)
{
    for(int i = 0; i < val->parent->u.object.length; i++)
    {
        if(val->parent->u.object.values[i].value == val)
        {
            return val->parent->u.object.values[i].name;
        }
    }
    return NULL;
}

/**
 * Recursive function used to compute the value of getJSRef. There are 3 cases
 * Case 1: val is the top element.
 * -> allocate the memory for the string (length + 5) and print "_TOP" to it.
 * -> return the string
 * Case 2: val->parent is an array
 * -> Get the index of val in its parent
 * -> Recurse, passing the parent and length + 2 + floor(log10(index of val))
 * -> Append "[index of val]" to the returned string and return.
 * Case 3: val->parent is an object
 * -> Get the name of val in its parent
 * -> Recurse, passing the parent and length + 1 + length of the name of val
 * -> Append ".name of val" to the returned string and return.
 * 
 * @see getJSRef(json_value)
 */
char *_jsRef(json_value *val, int length)
{
    if(val->parent == NULL)
    {
        char *ref = malloc(length + 5);
        sprintf(ref, "_TOP");
        return ref;
    }
    else if(val->parent->type == json_array)
    {
        size_t index = _jsGetIndex(val);
        char *ref = _jsRef(val->parent, length + _getIntWidth(index) + 2);
        sprintf(ref + strlen(ref), "[%d]", index);
        return ref;
    }
    else
    {
        char *propName = _jsGetName(val);
        char *ref = _jsRef(val->parent, length + strlen(propName) + 1);
        sprintf(ref + strlen(ref), ".%s", propName);
        return ref;
    }
}

/**
 * Gets a string representation of the given json_value. This representation
 * is how you would access it JavaScript. The root is represented as _TOP. An
 * example output would be "_TOP.commands[0]"
 * 
 * @param val The value whose representation is being retrieved
 * @returns A string representing the given json_value.
 */
char *getJSRef(json_value *val)
{
    return _jsRef(val, 0);
}

// Helpers for validate()
/**
 * Checks a given object for a property with a specific name and type.
 * If the property is not found or has the wrong type, it will logvf what
 * went wrong specifically.
 * 
 * @param obj the object being searched
 * @param name the name being searched for
 * @param type the desired type of the property
 * 
 * @pre obj->type is json_object
 * @returns true if the object contains a property with the correct type,
 *          false if the property does not exist or has the wrong type
 */
bool _checkObject(json_value *obj, const char *name, json_type type)
{
    char *ref = getJSRef(obj);
    logvf("INFO: Checking %s for required property %s of type %s...", ref, name, js_tstr(type));
    for (int i = 0; i < obj->u.object.length; i++)
    {
        if (strcmp(obj->u.object.values[i].name, name) == 0)
        {
            if (obj->u.object.values[i].value->type == type)
            {
                logvf("PASS\n");
                return true;
            }
            else
            {
                logvf("FAIL\n");
                char *tref = getJSRef(obj->u.object.values[i].value);
                _logTypeError(tref, type, obj->u.object.values[i].value->type);
                free(tref);
                return false;
            }
        }
    }
    logvf("FAIL\n");
    flogvf(stderr, "ERROR: Required property %s not found in %s\n", name, ref);
    free(ref);
    return false;
}

/**
 * Checks that all elements in a json array are of the same type.
 * 
 * @param arr the array being checked
 * @param type the type which all elements are verified against
 * 
 * @pre arr->type is json_array
 * @returns true if all elements match the given type.
 */
bool _checkArray(json_value *arr, json_type type)
{
    char *ref = getJSRef(arr);
    logvf("INFO: Checking type of all elements in %s...\n", ref);
    free(ref);
    for(int i = 0; i < arr->u.array.length; i++)
    {
        if(arr->u.array.values[i]->type != type)
        {
            ref = getJSRef(arr->u.array.values[i]);
            _logTypeError(ref, type, arr->u.array.values[i]->type);
            free(ref);
            return false;
        }
    }
    return true;
}

/**
 * Gets a property of a given name from a given json_object.
 * 
 * @param parent the parent object of the property being retrieved
 * @param name the name of the property being retrieved
 * 
 * @pre parent->type is json_object
 * @returns If the property is found, returns a pointer to the json_value with the given name.
 *          Otherwise, returns NULL
 */
json_value *_getProperty(json_value *parent, const char *name)
{
    for(int i = 0; i < parent->u.object.length; i++)
    {
        if(strcmp(parent->u.object.values[i].name, name) == 0)
        {
            return parent->u.object.values[i].value;
        }
    }
    return NULL;
}

bool _checkComExists(const char *name)
{
    #ifndef NO_DIR_VAL
    char *path = malloc(strlen(name) + 12);
    sprintf(path, "commands/%s.sh", name);
    bool exists = access(path, R_OK) != -1;
    free(path);
    return exists;
    #else
    return true;
    #endif
}

/**
 * Checks the given command array for duplicate command names.
 * Prompts the user if 1 or more duplicates are found.
 * 
 * @param arr The command array to be searched
 * 
 * @pre arr contains only objects with the property name, 
 *      which is a string. (Verified previously)
 * @returns true if there are no duplicates, or the user decides
 *          to ignore duplicates, otherwise, false.
 */
bool _checkNoDupes(json_value *arr)
{
    logvf("INFO: Checking for duplicate commands...\n");
    int dupeCount = 0;
    char *curr_name;
    for(int i = 0; i < arr->u.array.length - 1; i++)
    {
        curr_name = arr->u.array.values[i]->u.string.ptr;
        for(int j = i + 1; j < arr->u.array.length; j++)
        {
            if(strcmp(curr_name, arr->u.array.values[j]->u.string.ptr) == 0)
            {
                flogvf(stderr, "WARNING: There are 1 or more commands with the name %s.", curr_name);
                dupeCount++;
            }
        }
    }
    if(dupeCount != 0)
    {
        fprintf(stderr, "WARNING: 1 or more commands occurred more than once. Run with '-v' to see more\n");
        return prompt("Would you like to continue despite the duplicates?");
    }
    return true;
}

/**
 * Validates an element of _PACK.commands, ensuring the following
 * -> element.name is a string
 * -> element.phrases is an array of strings
 * -> if NO_DIR_VAL is not present, checks if "element.name".sh exists in commands/
 * 
 * @param element the element being validated
 * 
 * @pre element->type is object (earlier checks ensure this)
 * @returns true if the element passes, otherwise false
 */
bool _valCommand(json_value *element)
{
    if(!_checkObject(element, "name", json_string) || !_checkObject(element, "phrases", json_array))
    {
        char *ref = getJSRef(element);
        flogvf(stderr, "ERROR: commands validation failed at %s during property check\n", ref);
        free(ref);
        return false;
    }
    if(!_checkArray(_getProperty(element, "phrases"), json_string) || _getProperty(element, "phrases")->u.array.length == 0)
    {
        char *ref = getJSRef(element);
        flogvf(stderr, "ERROR: commands validation failed at %s during phrases check\n", ref);
        free(ref);
        return false;
    }
    if(!_checkComExists(_getProperty(element, "name")->u.string.ptr))
    {
        flogvf(stderr, "ERROR: Command does not exist in commands directory\n");
        return false;
    }
    return true;
}

bool validate(char *input, char **ns)
{
    printf("INFO: Validating pack.json contents.\n");
    json_value *json = json_parse(input, strlen(input));
    int warnCount = 0;


    // Step 1: Check top level is Object
    logvf("INFO: Verifying top level is object...");
    if (json->type == json_object)
    {
        logvf("PASS\n");
    }
    else
    {
        logvf("FAIL\n");
        char *ref = getJSRef(json);
        _logTypeError(ref, json_object, json->type);
        free(ref);
        return false;
    }
    // Step 2: verify top levels exist and have correct type
    logvf("INFO: Verfying required properties exist...\n");
    if (!(_checkObject(json, "name", json_string) && _checkObject(json, "description", json_string) && _checkObject(json, "namespace", json_string) && _checkObject(json, "commands", json_array)))
    {
        return false;
    }
    logvf("INFO: Required properties verified.\n");
    *ns = _getProperty(json, "namespace")->u.string.ptr;

    // Step 3: Validate namespace contents
    logvf("INFO: Validating content of namespace property...");
    const char *nsVal = _getProperty(json, "namespace")->u.string.ptr;
    int nsLen = strlen(nsVal);
    for(int i = 0; i < nsLen; i++)
    {
        if(!(nsVal[i] == '.' || isalnum(nsVal[i])))
        {
            logvf("FAIL\n");
            flogvf(stderr, "ERROR: Failed to validate namespace property. \"namespace\" must only contain alphanumeric characters and \'.\'\n");
            return false;
        }
    }
    logvf("PASS\n");

    // Step 4: Validate entries of command
    logvf("INFO: Validating all elements of commands property...\n");
    json_value *commands = _getProperty(json, "commands");
    for(int i = 0; i < commands->u.array.length; i++)
    {
        if(!_valCommand(commands->u.array.values[i]))
        {
            flogvf(stderr, "Validation failed at %s", commands->u.array.values[i]);
            return false;
        }
    }
    if(_checkNoDupes(commands))
    {
        logvf("INFO: Duplicate check complete.\n");
    }
    else
    {
        flogvf(stderr, "ERROR: Duplicate check failed.\n");
        return false;
    }
    logvf("INFO: Successfully validated all elements of commands\n");

    // Step 5: If deps exists, validate it
    logvf("INFO: Checking optional properties.\n");
    json_value *deps = _getProperty(json, "deps");
    if(deps != NULL && deps->type == json_object)
    {
        logvf("INFO: Found optional property 'deps'. Validating contents.\n");
        json_value *apt = _getProperty(deps, "apt");
        if(apt != NULL)
        {
            logvf("INFO: deps.apt found. Validating contents.\n");
            if(apt->type == json_array)
            {
                if(_checkArray(apt, json_string))
                {
                    logvf("INFO: deps.apt validated successfully.\n");
                }
                else
                {
                    flogvf(stderr, "WARNING: deps.apt contained non-string elements.\n");
                    warnCount++;
                }
            }
            else
            {
                flogvf(stderr, "ERROR: deps.apt was not an array. This will crash the installer\n");
                return false;
            }
        }
        json_value *pip = _getProperty(deps, "pip");
        if(pip != NULL)
        {
            logvf("INFO: deps.pip found. Validating contents.\n");
            if(pip->type == json_array)
            {
                if(_checkArray(pip, json_string))
                {
                    logvf("INFO: deps.pip validated successfully.\n");
                }
                else
                {
                    flogvf(stderr, "WARNING: deps.pip contained non-string elements. This may affect installation\n");
                    warnCount++;
                }
            }
            else
            {
                flogvf(stderr, "ERROR: deps.pip was not an array. This will crash the installer\n");
                return false;
            }
        }
        if(apt == NULL && pip == NULL)
        {
            if(deps->u.object.length == 0)
            {
                flogvf(stderr, "WARNING: deps exists but contains no properties. This will not affect installation\n");
                warnCount++;
            }
            else
            {
                flogvf(stderr, "WARNING: deps exists but has unrecognized properties. This will not affect installation\n");
                warnCount++;
            }
        }
    }
    // Check for final warnings
    if(deps != NULL)
    {
        if(deps->type != json_object)
        {
            flogvf(stderr, "WARNING: deps exists but is not an object. This may affect installation\n");
            warnCount++;
        }
        if(json->u.object.length > 5)
        {
            flogvf(stderr, "WARNING: Extra properties found in pack.json. This should not affect installation.\n");
            warnCount++;
        }
    }
    else 
    {
        if(json->u.object.length > 4)
        {
            flogvf(stderr, "WARNING: Extra properties found in pack.json. This should not affect installation.\n");
            warnCount++;
        }
    }

    // Final warning prompt
    if(warnCount > 0)
    {
        fprintf(stderr, "WARNING: %d warnings were raised. Re-run with '-v' to see details.\n");
        if(!prompt("Would you like to continue in spite of warnings?"))
        {
            fprintf(stderr, "ERROR: User cancelled due to warnings\n");
            return false;
        }
    }
    

    printf("INFO: pack.json validated successfully.\n");
    return true;
}
