#ifndef DIRMAP_H
#define DIRMAP_H

typedef struct _dir_node {
    char *path;
    struct _dir_node *next;
} dir_node;

/**
 * Adds a file to the given directory listing
 * 
 * @param list A node of the given directory listing. It does not matter where
 *             in the list the item is.
 * @param path The path of the node being added.
 */
dir_node *dir_addFile(dir_node *list, char *path);

/**
 * Gets the size of the given list
 * 
 * @param list the root node of the list
 * @returns the number of elements in the linked list
 */
int dir_listSize(dir_node *list);

/**
 * Frees all memory occupied by the given dirlist
 * 
 * @param list the head of the list
 */
void dir_delList(dir_node *list);

/**
 * Maps the directory given in dir path to a linked list containing
 * all the files and their relative paths do the directory above dirpath.
 * So, if dirPath is "commands", subfiles of commands would include
 * 
 * @param list A list which you can append new entries to. If you want
 *             a new list, just set this to NULL
 * @param dirPath The path to be scanned and added to the list.
 * 
 * @returns the root node of the file. 
 */
dir_node *dir_mapList(dir_node *list, char *dirPath);



#endif