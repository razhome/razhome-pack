#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include "util.h"
#include "json/json.h"
#include <string.h>
#include "validate.h"
#include <unistd.h>
#include "package.h"
#include "filemap.h"

int main(int argc, char *argv[])
{
    parseArgs(argc, argv);

    char *packfile = loadPackfile();
    char *namespace;

    if(validate(packfile, &namespace))
    {
        writePackage(namespace);
    }

    return 0;
}
