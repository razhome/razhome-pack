/**
 * validate.h
 * Handles validation of pack.json 
*/
#ifndef VALIDATE_H
#define VALIDATE_H
#include "json/json.h"
#include "util.h"
#include "stdbool.h"


bool validate(char *input, char **ns);

/**
 * Gets a string representation of the given json_value. This representation
 * is how you would access it JavaScript. The root is represented as _TOP. An
 * example output would be "_TOP.commands[0]"
 * 
 * @param val The value whose representation is being retrieved
 * @returns A string representing the given json_value.
 */
char *getJSRef(json_value *val);

#endif