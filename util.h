/**
 * util.h
 * Handles argument parsing and file-io
 */
#ifndef UTIL_H
#define UTIL_H
#include <stddef.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "json/json.h"

// Args passed at the command line
// more will be added
typedef struct
{
    bool verbose;
    bool quiet;
} ARGS;

/**
 * Parses arguments passed to the application at runtime.
 * Provides options for verbose and quiet output. ARGS are
 * available via getArgs()
 * 
 * @param argc the number of args passed to the program
 * @param argv the value of all the args passed to the program
 */
void parseArgs(int argc, char *argv[]);

/**
 * Parses a string which contains a command line switch
 * The program exits if the switch is not found. Switches
 * can be combined or separated, in typical GNU fashion.
 * For example rpack -v -q is the same as rpack -vq.
 * 
 * @param sw the switch being parsed
 */
void parseSwitch(char *sw);

/**
 * Gets the args that were initially passed to the program.
 * 
 * @pre parseArgs has been run
 * @returns the global args object.
 */
ARGS getArgs();

/**
 * Reads pack.json from the PWD. If it cannot
 * be found, raises an error and exits the program.
 * 
 * @returns a string containing the contents of pack.json
 */
char *loadPackfile();

/**
 * Asks the user for a yes or no reponse with the given
 * prompt. Will not show if quiet is set
 * 
 * @param prompt the text the user will be prompted with
 * 
 * @returns false if the user types 'n' or 'N', otherwise, or if
 *          quiet is set, true.
 */
bool prompt(char *prompt);

/**
 * Logs to stdout if verbose is not set, otherwise
 * does nothing. Syntax is the same as printf
 */
void logvf(char *format, ...);

/**
 * Logs to given FILE if verbose is set, otherwise
 * does nothing. Syntax is the same as fprintf
 */
void flogvf(FILE *_Stream, char *format, ...);

/**
 * Returns a string representation of a given json_type
 * 
 * @param type the type being translated
 */
const char *js_tstr(json_type type);

#endif